=== UW DGH | SEO Basics ===
Contributors: dghweb, jbleys
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Tested up to: 6.7
Requires at least: 3.2

Manage the basic Search Engine Optimization aspects of your website.

== Description ==
Manage the basic Search Engine Optimization aspects of your website.

- Integrates the default Robots Meta directive for "Search Engine Visibility"
- Add a custom robots.txt file
- Generate a sitemap XML file

== Changelog ==
**[unreleased]**

#### 0.5 / 2024-12-05
* Tested for WP 6.7
* Added NoAI Meta Tags option
* Added Disallow AI bots option to add disallow directives to robots.txt

#### 0.4.11 / 2024-06-07
* Bug fix: Fatal error: Uncaught Error: fwrite(): Argument #1 ($stream) must be of type resource, bool given

#### 0.4.10 / 2024-06-07
* Tested for WP 6.5
* Bug fix: Fatal error: Uncaught Error: fwrite(): Argument #1 ($stream) must be of type resource, bool given
* Fixed incorrect XSL href value

#### 0.4.9 / 2024-02-02
* Tested for WP 6.4
* Updated sitemap priority setting for post type page

#### 0.4.8 / 2023-06-26
* Tested for PHP 8.x
* Tested for WP 6.2

#### 0.4.7 / 2022-12-22
* Tested for WP 6.1
* Updated the actions array for the sitemap.

#### 0.4.6 / 2022-03-03
* Bug fixes.

#### 0.4.5 / 2022-02-23
* Bug fixes.

#### 0.4.4 / 2021-12-29
* Added new xsl stylesheet.

#### 0.4.3 / 2021-12-28
* Added more dynamic priority determination for page type.

#### 0.4.2 / 2021-12-27
* Added more dynamic priority determination for post type.

#### 0.4.1 / 2021-12-22
* Bug fix: updated xsl href path when Site Address and WP Address differ.

#### 0.4 / 2021-12-22
* Added XSL stylesheet for viewing the sitemap
* Updated readme, consolidated changelog

#### 0.3.5 / 2021-03-14
* Updated settings labels and UX.
* Removed "changefreq" tag and added "priority" tag to sitemap.

#### 0.3.4 / 2021-02-19
* Added "Root blog and 'wordpress' subdirectory" directive placeholder option for robots.txt.

#### 0.3.3 / 2020-08-27
* Updated plugin description

#### 0.3.2 / 2020-08-13
* Added plugin icon

#### 0.3.1 / 2020-08-12
* Added a "Disallow All" directive placeholder option for robots.txt.

#### 0.3 / 2020-08-11
* Added "Search engine visibility" option. This option synchronizes with the same option in the Reading Settings.

#### 0.2.8 / 2020-08-07
* Update plugin's banner image
* Added readme.txt file. This file retrieves plugin description and contributor list.

#### 0.2.7 / 2020-08-07
* update plugin's banner image
* updated incorrect changelog dates

#### 0.2.6 / 2020-08-07
* changelog document introduced
* add banner image to assets
* created loop for `add_actions` that create the sitemap

#### 0.2.5 / 2019‑06‑25
* Added `Allow: /wp-admin/admin-ajax.php` to placeholder content

#### 0.2.4 / 2019‑06‑07
* Use `get_home_path()` instead of `ABSPATH` to set file paths
* Added reference table to settings page

#### 0.2.3 / 2019‑05‑28
* Check if sitemap file exists before disabling option

#### 0.2.2 / 2019‑05‑10
* Change `home_url` to `site_url` to correctly point to the wordpress installation root where the sitemap file is stored

#### 0.2.1 / 2019‑05‑08
* Keep the robots.txt file when uninstalling the plugin
* Improved option descriptions

#### 0.2 / 2019‑05‑08
* added `delete_option`'s' to uninstall hook

#### 0.1 / 2019‑04‑23
* Initial commit
