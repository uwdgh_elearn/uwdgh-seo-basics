<?php
/*
Plugin Name:  UW DGH | SEO Basics
Plugin URI:   https://bitbucket.org/uwdgh_elearn/uwdgh-seo-basics
Description:  Manage the basic Search Engine Optimization aspects of your website.
Author:       Department of Global Health - University of Washington
Author URI:   https://depts.washington.edu/dghweb/
Version:      0.5
License:      GPLv2 or later
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  uwdgh-seo-basics
Domain Path:
Bitbucket Plugin URI:  https://bitbucket.org/uwdgh_elearn/uwdgh-seo-basics

"UW DGH | SEO Basics" is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

"UW DGH | SEO Basics" is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with "UW DGH | SEO Basics". If not, see https://www.gnu.org/licenses/gpl-2.0.html.
 */
?>
<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if( ! defined( 'UWDGH_SEO_Basics_PLUGIN' ) ) {
	define( 'UWDGH_SEO_Basics_PLUGIN', plugin_basename( __FILE__ ) ); // Plugin
}

if ( !class_exists( 'UWDGH_SEO_Basics' ) ) {

  class UWDGH_SEO_Basics {

    static $settingsupdated;
    static $sitemapfile;
    static $sitemappath;
    static $sitemapxsl;
    static $sitemapxslpath;
    static $sitemapxslhref;
    static $robotspath;

    /**
    * class initializaton
    */
    function __construct() {
      // Ensure get_home_path() is declared.
      require_once( ABSPATH . 'wp-admin/includes/file.php' );
      // IsPostback
      self::$settingsupdated = false;
      // file paths
      self::$sitemapfile = "uwdgh-seo-basics-sitemap.xml";
      self::$sitemappath = get_home_path() . self::$sitemapfile;
      self::$sitemapxsl = "uwdgh-seo-basics-sitemap.xsl";
      self::$sitemapxslpath = plugin_dir_path( __FILE__ ) . "xsl/" . self::$sitemapxsl;
      self::$sitemapxslhref = trailingslashit( plugins_url() ) . trailingslashit( dirname( UWDGH_SEO_Basics_PLUGIN ) ) . 'xsl/' . self::$sitemapxsl;
      self::$robotspath = get_home_path() . "robots.txt";
      // Add plugin settings link to Plugins page
      add_filter( 'plugin_action_links_'.UWDGH_SEO_Basics_PLUGIN, array( __CLASS__, 'uwdgh_seo_basics_add_settings_link' ) );
      // register plugin settings
      register_setting('uwdgh_seo_basics_options','uwdgh_seo_basics_options_sitemap_enabled', array('default' => 0,));
      register_setting('uwdgh_seo_basics_options','uwdgh_seo_basics_options_robots_enabled', array('default' => 0,));
      register_setting('uwdgh_seo_basics_options','uwdgh_seo_basics_options_robots_text', array('default' => '',));
      register_setting('uwdgh_seo_basics_options','uwdgh_seo_basics_options_robots_reset_default', array('default' => 0,));
      register_setting('uwdgh_seo_basics_options','uwdgh_seo_basics_options_robots_placeholder', array('default' => 'uwdgh_seo_basics_options_robots_text_default',));
      register_setting('uwdgh_seo_basics_options','uwdgh_seo_basics_options_robots_disallow_ai', array('default' => 0,));
      register_setting('uwdgh_seo_basics_options','uwdgh_seo_basics_options_blog_public', array('default' => '0',));
      register_setting('uwdgh_seo_basics_options','uwdgh_seo_basics_options_noai', array('default' => '0',));
      // add menu item
      add_action( 'admin_menu' , array( __CLASS__, 'uwdgh_seo_basics_settings' ) );
      // add function catching settings-updated
      add_action( 'admin_menu' , array( __CLASS__, 'uwdgh_seo_basics_settings_updated' ) );
      // add actions when to create the sitemap
      $_actions = array('save_post','publish_post','trashed_post','untrash_post','publish_page','trashed_page','untrash_page','updated_postmeta');
      foreach ($_actions as $_action) {
        add_action( $_action, array( __CLASS__, 'uwdgh_seo_basics_create_sitemap' ) );
      }
      // register deactivation hook
      if( function_exists('register_deactivation_hook') )
        register_deactivation_hook(__FILE__,array( __CLASS__, 'uwdgh_seo_basics_deactivate' ));
      // register uninstall hook
      if( function_exists('register_uninstall_hook') )
        register_uninstall_hook(__FILE__,array( __CLASS__, 'uwdgh_seo_basics_uninstall' ));

      // add admin js
      add_action('admin_enqueue_scripts', array( __CLASS__, 'uwdgh_seo_basics_admin_enqueue' ));

      // implement hook wp_head
      add_action('wp_head', array( __CLASS__, 'uwdgh_seo_basics_add_meta_tags'));
      
    }

    /**
    * Enqueue admin js
    */
    static function uwdgh_seo_basics_admin_enqueue($hook) {
      // Only add to this plugin's admin page.
      if ('settings_page_uwdgh-seo-basics' != $hook) {
          return;
      }
      wp_enqueue_script( 'uwdgh_seo_basics_admin', plugin_dir_url(__FILE__) . '/admin/js/uwdgh-seo-basics-admin.js', array('jquery') );
    }

    /**
    * Add settings link
    */
    static function uwdgh_seo_basics_add_settings_link( $links ) {
      $settings_link = '<a href="options-general.php?page=uwdgh-seo-basics">' . __( 'Settings' ) . '</a>';
      array_push( $links, $settings_link );
      return $links;
    }

    /**
    * Is Postback from submitting the option form
    */
    static function uwdgh_seo_basics_settings_updated() {
      if(isset($_GET['settings-updated'])){
        self::$settingsupdated = $_GET['settings-updated'];
      }
    }

    /**
    * Admin menu item
    */
    static function uwdgh_seo_basics_settings() {
      add_options_page('UW DGH | SEO Basics','UW DGH | SEO Basics','manage_options','uwdgh-seo-basics',array( __CLASS__, 'uwdgh_seo_basics_options_page' ));
    }

    /**
    * Options form
    */
    static function uwdgh_seo_basics_options_page() {
      // update Search Engine Visibility
      if (self::$settingsupdated) {
        if (get_option('uwdgh_seo_basics_options_blog_public')=='0') {
          update_option('blog_public', '0');
        } else {
          update_option('blog_public', '1');
        }
      } else {
        // sync with the site's "blog_public" option
        if ( get_option('uwdgh_seo_basics_options_blog_public')==false && get_option( 'blog_public' )=='0' ) {
          update_option('uwdgh_seo_basics_options_blog_public', get_option( 'blog_public' ));
        } elseif ( get_option('uwdgh_seo_basics_options_blog_public')=='0' && get_option( 'blog_public' )=='1') {
          update_option('uwdgh_seo_basics_options_blog_public', get_option( 'blog_public' ));
        }
      }

      // update NoAI
      if (self::$settingsupdated)
        update_option('uwdgh_seo_basics_options_noai', get_option( 'uwdgh_seo_basics_options_noai' ));

      // reset option if xml does not exist and if not a postback
      if ( file_exists(self::$sitemappath)==false && self::$settingsupdated==false && get_option('uwdgh_seo_basics_options_sitemap_enabled') ) {
        update_option( 'uwdgh_seo_basics_options_sitemap_enabled', false);
      }
      if ( get_option('uwdgh_seo_basics_options_sitemap_enabled') ) {
        // write sitemap.xml
        self::uwdgh_seo_basics_create_sitemap();
      } else {
        // remove sitemap.xml
        if (file_exists( self::$sitemappath ))
          unlink( self::$sitemappath );
      }

      // load from robots.txt file and update option settings if not a postback
      if ( file_exists(self::$robotspath) && self::$settingsupdated==false ) {
        update_option( 'uwdgh_seo_basics_options_robots_enabled', true );
        update_option( 'uwdgh_seo_basics_options_robots_text', self::uwdgh_seo_basics_get_robotstxt() );
      }
      // add disallow AI text to existing robots.txt
      if ( file_exists(self::$robotspath) && self::$settingsupdated && get_option('uwdgh_seo_basics_options_robots_disallow_ai') ) {
        update_option( 'uwdgh_seo_basics_options_robots_text', get_option('uwdgh_seo_basics_options_robots_text') . self::uwdgh_seo_basics_options_robots_text_disallow_ai() );
      }
      if ( get_option('uwdgh_seo_basics_options_robots_enabled') ) {
        // reset to placeholder
        if ( get_option('uwdgh_seo_basics_options_robots_reset_default') && self::$settingsupdated ) {
          // update option to placeholder text
          switch (get_option('uwdgh_seo_basics_options_robots_placeholder')) {
            case 'uwdgh_seo_basics_options_robots_text_default':
              update_option( 'uwdgh_seo_basics_options_robots_text', self::uwdgh_seo_basics_options_robots_text_default());
              break;
            case 'uwdgh_seo_basics_options_robots_text_subdir':
              update_option( 'uwdgh_seo_basics_options_robots_text', self::uwdgh_seo_basics_options_robots_text_subdir());
              break;
            case 'uwdgh_seo_basics_options_robots_text_disallow_all':
              update_option( 'uwdgh_seo_basics_options_robots_text', self::uwdgh_seo_basics_options_robots_text_disallow_all());
              break;
            default:
              // code...
              break;
          }
          // add disallow AI text
          if ( get_option('uwdgh_seo_basics_options_robots_disallow_ai') && self::$settingsupdated ) {
            update_option( 'uwdgh_seo_basics_options_robots_text', get_option('uwdgh_seo_basics_options_robots_text') . self::uwdgh_seo_basics_options_robots_text_disallow_ai() );
          }
          // reset the value for the 'reset' and 'disallow AI bots' checkbox back to 0
          update_option('uwdgh_seo_basics_options_robots_reset_default', false);
          update_option('uwdgh_seo_basics_options_robots_disallow_ai', false);
        }
        // write option value to file on postback
        if (self::$settingsupdated)
          self::uwdgh_seo_basics_create_robotstxt();

      } else {
        // remove robots.txt
        if ( file_exists(self::$robotspath) && self::$settingsupdated )
          unlink( self::$robotspath );
      }
      ?>
      <div class="wrap">
        <h2><?php _e('UW DGH | SEO Basics','uwdgh-seo-basics');?></h2>
        <p><?php _e('Here you can set or edit the fields needed for the plugin.','uwdgh-seo-basics');?></p>
        <form action="options.php" method="post" id="uwdgh-seo-basics-options-form">
          <?php settings_fields('uwdgh_seo_basics_options'); ?>
          <table class="form-table">
              <tr class="odd" valign="top">
                <th scope="row">
                  <label for="uwdgh_seo_basics_options_blog_public">
                  <?php _e( 'Search engine visibility','uwdgh-seo-basics' ); ?>
                  </label>
                </th>
                <td>
                <input name="uwdgh_seo_basics_options_blog_public" type="checkbox" id="uwdgh_seo_basics_options_blog_public" value="0" <?php checked('0', get_option('uwdgh_seo_basics_options_blog_public'), true ); ?> />
                   <?php _e( 'Discourage search engines from indexing this site' ); ?>
                    <p class="description"><?php _e( 'It is up to search engines to honor this request.' ); ?><br>
                    This option synchronizes with the same option in the <a href="options-reading.php"><?php _e( 'Reading Settings' ) ?></a>.<br>
                    It adds the following robots meta directive to the <code>head</code> section of each html page: <code>&lt;meta name="robots" content="noindex,nofollow"&gt;</code></p>
              </td>
            </tr>
              <tr class="even" valign="top">
                <th scope="row">
                  <label for="uwdgh_seo_basics_options_noai">
                  <?php _e( 'NoAI Meta Tags','uwdgh-seo-basics' ); ?>
                  </label>
                </th>
                <td>
                <input name="uwdgh_seo_basics_options_noai" type="checkbox" id="uwdgh_seo_basics_options_noai" value="0" <?php checked('0', get_option('uwdgh_seo_basics_options_noai'), true ); ?> />
                   <?php _e( 'Enable NoAI meta tags.' ); ?>
                    <p class="description">Tell AI systems not to use your content without your consent to train their models.<br>
                    It adds the following robots meta directives to the <code>head</code> section of each html page:<br>
                    <code>&lt;meta name="robots" content="noai, noimageai"&gt;</code><br>
                    <code>&lt;meta name="CCBot" content="nofollow"&gt;</code><br>
                    <code>&lt;meta name="tdm-reservation" content="1"&gt;</code></p>
              </td>
            </tr>
            <tr class="odd" valign="top">
              <th scope="row">
                <label for="uwdgh_seo_basics_options_sitemap_enabled">
                  <?php _e('XML Sitemap','uwdgh-seo-basics');?>
                </label>
              </th>
              <td>
                <input type="checkbox" id="uwdgh_seo_basics_options_sitemap_enabled" name="uwdgh_seo_basics_options_sitemap_enabled"  value="1" <?php checked(1, get_option('uwdgh_seo_basics_options_sitemap_enabled'), true); ?> />
                <?php _e( 'Create ' . self::$sitemapfile ); ?>
                <p class="description">
                The file will be created at the following location: <code><?php echo(  self::$sitemappath ); ?></code><br>
                Saving this setting unchecked will delete the file.<br>
                Uninstalling the plugin will delete the file.<br>
                <?php if (file_exists(self::$sitemappath)) : ?>
                  View the sitemap: <a href="<?php _e(home_url() . '/' . self::$sitemapfile) ?>" target="_blank"><?php _e(home_url() . '/' . self::$sitemapfile) ?></a>
                <?php endif; ?>
                </p>
              </td>
            </tr>
            <tr class="even" valign="top">
              <th scope="row">
                <label for="uwdgh_seo_basics_options_robots_enabled">
                  <?php _e('Robots directives','uwdgh-seo-basics');?>
                </label>
              </th>
              <td>
                <input type="checkbox" id="uwdgh_seo_basics_options_robots_enabled" name="uwdgh_seo_basics_options_robots_enabled"  value="1" <?php checked(1, get_option('uwdgh_seo_basics_options_robots_enabled'), true); ?> />
                <?php _e( 'Create robots.txt' ); ?>
                <p class="description">
                The file will be created at the following location: <code><?php echo(  self::$robotspath ); ?></code><br>
                Check the box below the text field to use the placeholder text when saving the setting.<br>
                <strong>Note!</strong> Saving this setting unchecked will delete the existing robots.txt file without making a backup.<br>
                Uninstalling the plugin will <strong>not</strong> delete an existing robots.txt file.</p>
              </td>
            </tr>
            <tr class="odd" valign="top">
              <th scope="row">
              </th>
              <td>
                <div>
                  <label for="uwdgh_seo_basics_options_robots_text">
                    <?php _e('robots.txt:','uwdgh-seo-basics');?>
                  </label>
                </div>
                <textarea id="uwdgh_seo_basics_options_robots_text" name="uwdgh_seo_basics_options_robots_text" rows="16" cols="128" placeholder="<?php echo self::uwdgh_seo_basics_options_robots_text_default(); ?>"><?php echo self::uwdgh_seo_basics_get_robotstxt(); ?></textarea>
                <br>
                <label>
                <input type="checkbox" id="uwdgh_seo_basics_options_robots_reset_default" name="uwdgh_seo_basics_options_robots_reset_default"  value="1" <?php checked(1, get_option('uwdgh_seo_basics_options_robots_reset_default'), true); ?> />
                 Use placeholder</label> <p class="description" style="font-style: italic;">option will uncheck automatically after changes are saved</p>
                <p>Select a placeholder value:</p>
                <p title="<?php echo self::uwdgh_seo_basics_options_robots_text_default(); ?>"><label><input type="radio" name="uwdgh_seo_basics_options_robots_placeholder" value="uwdgh_seo_basics_options_robots_text_default" <?php checked('uwdgh_seo_basics_options_robots_text_default', get_option('uwdgh_seo_basics_options_robots_placeholder') ); ?> /> Default</label>
                  <p class="description" style="margin-left:25px;">directives for a common WordPress installation, including a Sitemap directive</p></p>
                <p title="<?php echo self::uwdgh_seo_basics_options_robots_text_subdir(); ?>"><label><input type="radio" name="uwdgh_seo_basics_options_robots_placeholder" value="uwdgh_seo_basics_options_robots_text_subdir" <?php checked('uwdgh_seo_basics_options_robots_text_subdir', get_option('uwdgh_seo_basics_options_robots_placeholder') ); ?> /> Root blog and 'wordpress' subdirectory</label>
                  <p class="description" style="margin-left:25px;">directives for WordPress installed as root blog with wordpress files in a subdirectory</p></p>
                <p title="<?php echo self::uwdgh_seo_basics_options_robots_text_disallow_all(); ?>"><label><input type="radio" name="uwdgh_seo_basics_options_robots_placeholder" value="uwdgh_seo_basics_options_robots_text_disallow_all" <?php checked('uwdgh_seo_basics_options_robots_text_disallow_all', get_option('uwdgh_seo_basics_options_robots_placeholder') ); ?> /> Disallow All</label>
                  <p class="description" style="margin-left:25px;">standard directive to disallow all crawling</p></p>
              </td>
            </tr>
            <tr class="even" valign="top">
              <th scope="row">
                <label for="uwdgh_seo_basics_options_robots_disallow_ai">
                  <?php _e('Disallow AI bots','uwdgh-seo-basics');?>
                </label>
              </th>
              <td>
                <input type="checkbox" id="uwdgh_seo_basics_options_robots_disallow_ai" name="uwdgh_seo_basics_options_robots_disallow_ai"  value="1" <?php checked(1, get_option('uwdgh_seo_basics_options_robots_disallow_ai'), true); ?> />
                <?php _e( 'Add Disallow directives for AI bots to robots.txt' ); ?>
                <p class="description" style="font-style: italic;">option will uncheck automatically after changes are saved</p>
                <p class="description">
                This option will add <code>Disallow: /</code> directives to the existing robots.txt file for a a variety of AI bots.<br>
                  <details><summary>List of AI bot directives</summary>
                  <textarea disabled rows="12" cols="64"><?php echo self::uwdgh_seo_basics_options_robots_text_disallow_ai(); ?></textarea>
                  </details>
                </p>
              </td>
            </tr>
          </table>
          <?php submit_button(); ?>
        </form>
        <hr>
        <table>
          <thead>
            <tr>
              <th colspan="0" style="text-align: initial;">System paths reference table</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><code title="<?php _e("Returns the path on the remote filesystem of ABSPATH.","default"); ?>">ABSPATH</code></td>
              <td><var><?php echo(ABSPATH); ?></var></td>
            </tr>
            <tr>
              <td><code title="<?php _e("Gets the absolute filesystem path to the root of the WordPress installation.","default"); ?>">get_home_path()</code></td>
              <td><var><?php echo(get_home_path()); ?></var></td>
            </tr>
            <tr>
              <td><code title="<?php _e("Retrieves the URL for the current site where WordPress application files (e.g. wp-blog-header.php or the wp-admin/ folder) are accessible.","default"); ?>">site_url()</code></td>
              <td><var><?php echo(site_url()); ?></var></td>
            </tr>
            <tr>
              <td><code title="<?php _e("Retrieves the URL for the current site where the front end is accessible.","default"); ?>">home_url()</code></td>
              <td><var><?php echo(home_url()); ?></var></td>
            </tr>
            <tr>
              <td><code title="<?php _e("Retrieves stylesheet directory path for current theme.","default"); ?>">get_stylesheet_directory()</code></td>
              <td><var><?php echo(get_stylesheet_directory()); ?></var></td>
            </tr>
            <tr>
              <td><code title="<?php _e("Retrieves stylesheet directory URI for current theme.","default"); ?>">get_stylesheet_directory_uri()</code></td>
              <td><var><?php echo(get_stylesheet_directory_uri()); ?></var></td>
            </tr>
            <tr>
              <td><code title="<?php _e("Retrieves template directory path for current theme.","default"); ?>">get_template_directory()</code></td>
              <td><var><?php echo(get_template_directory()); ?></var></td>
            </tr>
            <tr>
              <td><code title="<?php _e("Retrieves template directory URI for current theme.","default"); ?>">get_template_directory_uri()</code></td>
              <td><var><?php echo(get_template_directory_uri()); ?></var></td>
            </tr>
            <tr>
              <td><code title="<?php _e("Get the filesystem directory path (with trailing slash) for the plugin __FILE__ passed in.","default"); ?>">plugin_dir_path( __FILE__ )</code></td>
              <td><var><?php echo(plugin_dir_path( __FILE__ )); ?></var></td>
            </tr>
          </tbody>
        </table>
      </div>
      <?php
    }

    /**
     * Add custom meta tags
     * 
     * copied from https://github.com/healsdata/ai-training-opt-out/blob/main/meta-tags.html
     */
    static function uwdgh_seo_basics_add_meta_tags() {
      
      // NoAI
      if ( 0 == get_option( 'uwdgh_seo_basics_options_noai' ) ) {
        echo '<!-- Begin NoAI tags -->';
        // Used by DeviantArt, ArtStation, etc. based on opt-in or opt-out
        echo '<meta name="robots" content="noai, noimageai">';
        // The Common Crawl dataset. Used by GPT-3 (and GPT-3.5) and available for others.
        echo '<meta name="CCBot" content="nofollow">';
        // TDM (Text and Data Mining) Metadata in HTML Content
        echo '<meta name="tdm-reservation" content="1">';
        echo '<!-- End NoAI tags -->';
      }

    }

    /**
    * Create the sitemap
    */
    static function uwdgh_seo_basics_create_sitemap() {

			try {

				$postsForSitemap = get_posts(array(
					'numberposts' => -1,
					'orderby' => 'modified',
					'post_type'  => array( 'post', 'page' ),
					'order'    => 'DESC'
				));

				$sitemap = '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL;
				if (file_exists(self::$sitemapxslpath))
					$sitemap .= '<?xml-stylesheet type="text/xsl" href="'.self::$sitemapxslhref.'"?>' . PHP_EOL;
				$sitemap .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . PHP_EOL;

				foreach( $postsForSitemap as $post ) {
					setup_postdata( $post );

					$date_created = date_create($post->post_date);
					$date_modified = date_create($post->post_modified);
					$now = date_create();

					$postdate = explode( " ", $post->post_modified );

					$sitemap .= '<url>'. PHP_EOL .
						'<loc>' . get_permalink( $post->ID ) . '</loc>' . PHP_EOL .
						'<lastmod>' . $postdate[0] . '</lastmod>' . PHP_EOL;
						// set priority
						if ( ( get_option( 'show_on_front' )=='page' ) && ( $post->ID == get_option( 'page_on_front' ) ) ) {
							$sitemap .= '<priority>1.0</priority>' . PHP_EOL;
						} elseif ($post->ID == get_option( 'page_for_posts' )) {
							if ( get_option( 'show_on_front' )=='posts' ) {
								$sitemap .= '<priority>1.0</priority>' . PHP_EOL;
							} else {
								$sitemap .= '<priority>0.8</priority>' . PHP_EOL;
							}
						} elseif ($post->post_type == 'page') {
							if ( $post->post_parent == 0 ) {
								// between 0 and 2 years old
								if ( (date_diff($date_created, $now)->days <= 730) ) {
									// was the page updated in the last 6 months?
									if ( (date_diff($date_modified, $now)->days <= 183) ) {
										$sitemap .= '<priority>0.8</priority>' . PHP_EOL;
									} else {
										$sitemap .= '<priority>0.7</priority>' . PHP_EOL;
									}
								} else {
									// was the page updated in the last 6 months?
									if ( (date_diff($date_modified, $now)->days <= 183) ) {
										$sitemap .= '<priority>0.7</priority>' . PHP_EOL;
									} else {
										$sitemap .= '<priority>0.5</priority>' . PHP_EOL;
									}
								}
							} else {
								// between 0 and 2 years old
								if ( (date_diff($date_created, $now)->days <= 730) ) {
									// was the page updated in the last 6 months?
									if ( (date_diff($date_modified, $now)->days <= 183) ) {
										$sitemap .= '<priority>0.7</priority>' . PHP_EOL;
									} else {
										$sitemap .= '<priority>0.6</priority>' . PHP_EOL;
									}
								} else {
									// was the page updated in the last 6 months?
									if ( (date_diff($date_modified, $now)->days <= 183) ) {
										$sitemap .= '<priority>0.6</priority>' . PHP_EOL;
									} else {
										$sitemap .= '<priority>0.4</priority>' . PHP_EOL;
									}
								}
							}
						} else {
							if ($post->post_type == 'post') {
								if ( (date_diff($date_created, $now)->days <= 14) ) {
									// less than 2 weeks
									$sitemap .= '<priority>0.7</priority>' . PHP_EOL;
								} elseif ((date_diff($date_created, $now)->days > 14) && (date_diff($date_created, $now)->days <= 31)) {
									// between 2 weeks and 1 month old
									$sitemap .= '<priority>0.6</priority>' . PHP_EOL;
								} elseif ((date_diff($date_created, $now)->days > 31) && (date_diff($date_created, $now)->days <= 93)) {
									// between 1 and 3 months old
									if ( (date_diff($date_modified, $now)->days <= 31) ) {
										$sitemap .= '<priority>0.6</priority>' . PHP_EOL;
									} else {
										$sitemap .= '<priority>0.5</priority>' . PHP_EOL;
									}
								} elseif ((date_diff($date_created, $now)->days > 93) && (date_diff($date_created, $now)->days <= 365)) {
									// between 3 months and 1 year old
									if ( (date_diff($date_modified, $now)->days <= 31) ) {
										$sitemap .= '<priority>0.5</priority>' . PHP_EOL;
									} else {
										$sitemap .= '<priority>0.4</priority>' . PHP_EOL;
									}
								} elseif ((date_diff($date_created, $now)->days > 365) && (date_diff($date_created, $now)->days <= 730)) {
									// between 1 and 2 years old
									if ( (date_diff($date_modified, $now)->days <= 31) ) {
										$sitemap .= '<priority>0.5</priority>' . PHP_EOL;
									} else {
										$sitemap .= '<priority>0.3</priority>' . PHP_EOL;
									}
								} elseif ((date_diff($date_created, $now)->days > 730) && (date_diff($date_created, $now)->days <= 1825)) {
									// between 2 and 5 years old
									if ( (date_diff($date_modified, $now)->days <= 31) ) {
										$sitemap .= '<priority>0.5</priority>' . PHP_EOL;
									} else {
										$sitemap .= '<priority>0.2</priority>' . PHP_EOL;
									}
								} else {
									// older than 5 years
									if ( (date_diff($date_modified, $now)->days <= 31) ) {
										$sitemap .= '<priority>0.5</priority>' . PHP_EOL;
									} else {
										$sitemap .= '<priority>0.1</priority>' . PHP_EOL;
									}
								}
							} else {
								$sitemap .= '<priority>0.5</priority>' . PHP_EOL;
							}
						}
					$sitemap .= '</url>' . PHP_EOL;
				}

				$sitemap .= '</urlset>';

				$fp = fopen( self::$sitemappath, 'w' );
				if ( false === $fp ) {
					do_action( 'qm/info', 'Unable to write to: ' . self::$sitemappath );
					exit;
				}
				
				fwrite( $fp, $sitemap );
				fclose( $fp );
			
			} catch (\Exception $e) {

				do_action( 'qm/error', $e->getMessage() );

			}
    }

    /**
     * write the option value to the robots.txt file
     */
    static function uwdgh_seo_basics_create_robotstxt() {
			try {

				$fp = fopen( self::$robotspath, 'w' );
				if ( false === $fp ) {
					do_action( 'qm/info', 'Unable to write to: ' . self::$robotspath );
					exit;
				}
				fwrite( $fp, get_option('uwdgh_seo_basics_options_robots_text') );
				fclose( $fp );

			} catch (\Exception $e) {

				do_action( 'qm/error', $e->getMessage() );

			}
		}

    /**
     * return text of the robots.txt file
     */
    static function uwdgh_seo_basics_get_robotstxt() {
			$robotstxt = '';
      if (file_exists( self::$robotspath )) {
        $robotstxt = file_get_contents(self::$robotspath);
      }
      return $robotstxt;
    }

    /**
     * Returns standard directives for robots.txt
     * 'Allow: /wp-admin/admin-ajax.php' (allows bots to fetch and render JavaScript, CSS, etc.)
     */
    static function uwdgh_seo_basics_options_robots_text_default() {
      $robotstxt = '### UW DGH | SEO Basics  ###' . PHP_EOL .
      '### placeholder robots.txt ###' . PHP_EOL .
      '' . PHP_EOL .
      'User-Agent: *' . PHP_EOL .
      'Allow: /wp-content/uploads/' . PHP_EOL .
      'Disallow: /wp-content/plugins/' . PHP_EOL .
      'Disallow: /wp-admin/' . PHP_EOL .
      'Allow: /wp-admin/admin-ajax.php' . PHP_EOL .
      'Disallow: /readme.html' . PHP_EOL .
      'Disallow: /refer/' . PHP_EOL .
      '' . PHP_EOL .
      'Sitemap: ' . home_url() . '/' . self::$sitemapfile . PHP_EOL ;
      return $robotstxt;
    }

      /**
       * Returns directives for a root blog and wordpress subdirectory for robots.txt
       * 'Allow: /wp-admin/admin-ajax.php' (allows bots to fetch and render JavaScript, CSS, etc.)
       */
      static function uwdgh_seo_basics_options_robots_text_subdir() {
        $robotstxt = '### UW DGH | SEO Basics  ###' . PHP_EOL .
        '### placeholder robots.txt ###' . PHP_EOL .
        '' . PHP_EOL .
        'User-Agent: *' . PHP_EOL .
        'Disallow: /wordpress/' . PHP_EOL .
        'Allow: /wordpress/wp-content/uploads/' . PHP_EOL .
        'Allow: /wordpress/wp-admin/admin-ajax.php' . PHP_EOL .
        '' . PHP_EOL .
        'Sitemap: ' . home_url() . '/' . self::$sitemapfile . PHP_EOL ;
        return $robotstxt;
      }

    /**
     * Returns the default disallow crawling directive for robots.txt
     */
    static function uwdgh_seo_basics_options_robots_text_disallow_all() {
      $robotstxt = '### UW DGH | SEO Basics  ###' . PHP_EOL .
      '### placeholder robots.txt ###' . PHP_EOL .
      '' . PHP_EOL .
      'User-Agent: *' . PHP_EOL .
      'Disallow: /' . PHP_EOL ;
      return $robotstxt;
    }

    /**
     * returns directives that disallow AI bots
     * 
     * copied from https://github.com/healsdata/ai-training-opt-out/blob/main/robots.txt
     */
    static function uwdgh_seo_basics_options_robots_text_disallow_ai() {
      $robotstxt_noai = <<<DISALLOW_AI
      \n
      ### Disallow AI bots ###
      # The example for img2dataset, although the default is *None*
      User-agent: img2dataset
      Disallow: /

      # Brandwatch - "AI to discover new trends"
      User-agent: magpie-crawler
      Disallow: /

      # webz.io - they sell data for training LLMs.
      User-agent: Omgilibot
      Disallow: /

      # Items below were sourced from darkvisitors.com
      # Categories included: "AI Data Scraper", "AI Assistant", "AI Search Crawler", "Undocumented AI Agent"

      # AI Search Crawler
      # https://darkvisitors.com/agents/amazonbot

      User-agent: Amazonbot
      Disallow: /

      # Undocumented AI Agent
      # https://darkvisitors.com/agents/anthropic-ai

      User-agent: anthropic-ai
      Disallow: /

      # AI Search Crawler
      # https://darkvisitors.com/agents/applebot

      User-agent: Applebot
      Disallow: /

      # AI Data Scraper
      # https://darkvisitors.com/agents/applebot-extended

      User-agent: Applebot-Extended
      Disallow: /

      # AI Data Scraper
      # https://darkvisitors.com/agents/bytespider

      User-agent: Bytespider
      Disallow: /

      # AI Data Scraper
      # https://darkvisitors.com/agents/ccbot

      User-agent: CCBot
      Disallow: /

      # AI Assistant
      # https://darkvisitors.com/agents/chatgpt-user

      User-agent: ChatGPT-User
      Disallow: /

      # Undocumented AI Agent
      # https://darkvisitors.com/agents/claude-web

      User-agent: Claude-Web
      Disallow: /

      # AI Data Scraper
      # https://darkvisitors.com/agents/claudebot

      User-agent: ClaudeBot
      Disallow: /

      # Undocumented AI Agent
      # https://darkvisitors.com/agents/cohere-ai

      User-agent: cohere-ai
      Disallow: /

      # AI Data Scraper
      # https://darkvisitors.com/agents/diffbot

      User-agent: Diffbot
      Disallow: /

      # AI Data Scraper
      # https://darkvisitors.com/agents/facebookbot

      User-agent: FacebookBot
      Disallow: /

      # AI Data Scraper
      # https://darkvisitors.com/agents/google-extended

      User-agent: Google-Extended
      Disallow: /

      # AI Data Scraper
      # https://darkvisitors.com/agents/gptbot

      User-agent: GPTBot
      Disallow: /

      # AI Data Scraper
      # https://darkvisitors.com/agents/omgili

      User-agent: omgili
      Disallow: /

      # AI Search Crawler
      # https://darkvisitors.com/agents/perplexitybot

      User-agent: PerplexityBot
      Disallow: /

      # AI Search Crawler
      # https://darkvisitors.com/agents/youbot

      User-agent: YouBot
      Disallow: /
      \n
      DISALLOW_AI;
      return $robotstxt_noai;
    }

    /**
    * Dispose plugin option upon plugin deactivation
    */
    static function uwdgh_seo_basics_deactivate() {
      // nothing to do here
    }

    /**
    * Dispose plugin option upon plugin deletion
    */
    static function uwdgh_seo_basics_uninstall() {
      // remove options
      delete_option('uwdgh_seo_basics_options_sitemap_enabled');
      delete_option('uwdgh_seo_basics_options_robots_enabled');
      delete_option('uwdgh_seo_basics_options_robots_text');
      delete_option('uwdgh_seo_basics_options_robots_reset_default');
      // delete sitemap file
      if (file_exists( self::$sitemappath ))
        unlink( self::$sitemappath );
    }

  }

  New UWDGH_SEO_Basics;

}
