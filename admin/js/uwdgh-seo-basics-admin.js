// The document ready event executes when the HTML-Document is loaded
// and the DOM is ready.
jQuery(document).ready(function( $ ) {

  // robots.txt
  $("textarea[name=uwdgh_seo_basics_options_robots_text]").prop("disabled", true);
  $("input[name=uwdgh_seo_basics_options_robots_reset_default]").prop("disabled", true).prop( "checked", false );
  $("input[name=uwdgh_seo_basics_options_robots_disallow_ai]").prop("disabled", true).prop( "checked", false );
  if ($('#uwdgh_seo_basics_options_robots_enabled').prop('checked')) {
    $("textarea[name=uwdgh_seo_basics_options_robots_text]").removeAttr("disabled");
    $("input[name=uwdgh_seo_basics_options_robots_reset_default]").removeAttr("disabled").prop( "checked", false );
    $("input[name=uwdgh_seo_basics_options_robots_disallow_ai]").removeAttr("disabled").prop( "checked", false );
  }
  // toggle robots.txt textarea
  $(document).on("click", "#uwdgh_seo_basics_options_robots_enabled", function(e){
    // toggle checked status
    if (e.currentTarget.checked) {
      $("textarea[name=uwdgh_seo_basics_options_robots_text]").removeAttr("disabled");
      $("input[name=uwdgh_seo_basics_options_robots_reset_default]").removeAttr("disabled")
      $("input[name=uwdgh_seo_basics_options_robots_disallow_ai]").removeAttr("disabled")
    } else {
      $("textarea[name=uwdgh_seo_basics_options_robots_text]").prop("disabled", true);
      $("input[name=uwdgh_seo_basics_options_robots_reset_default]").prop("disabled", true).prop( "checked", false );
      $("input[name=uwdgh_seo_basics_options_robots_disallow_ai]").prop("disabled", true).prop( "checked", false );
    }
  });
  // disable radio buttons
  $("input[name=uwdgh_seo_basics_options_robots_placeholder]").prop("disabled", true);
  // 'Use placeholder' checkbox click event
  $(document).on("click", "#uwdgh_seo_basics_options_robots_reset_default", function(e){
    // toggle checked status
    if (e.currentTarget.checked) {
      $("input[name=uwdgh_seo_basics_options_robots_placeholder]").removeAttr("disabled");
    } else {
      $("input[name=uwdgh_seo_basics_options_robots_placeholder]").prop("disabled", true);
    }
  });

})

// The window load event executes after the document ready event,
// when the complete page is fully loaded.
jQuery(window).load(function () {

})
