# README #

This README document contains steps necessary to use the _UW DGH | SEO Basics_ WordPress plugin

## What is this repository for? ##

Manage the basic Search Engine Optimization aspects of your website.

* Integrates the default Robots Meta directive for "Search Engine Visibility"
* Create a customizable robots.txt file
* Automatically generates a sitemap XML file

## How do I get set up? ##

* Navigate to your wp-content/plugins/ folder
* `git clone https://bitbucket.org/uwdgh_elearn/uwdgh-seo-basics`
Alternatively you can use the [Github Updater](https://github.com/afragen/github-updater) plugin to manage themes and plugins not hosted on WordPress.org.

## Who do I talk to? ##

* dghweb@uw.edu
